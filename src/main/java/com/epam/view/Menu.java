package com.epam.view;

import com.epam.model.Renderable;

import java.util.List;

/**
 * Implementing this interface allows an object
 * to display menu.
 */
public interface Menu {

    /**
     * Represent single data input.
     * @param renderable object that have defined certain representation
     * @return user input
     */
    String enter(Renderable renderable);

    /**
     * Represents menu with multiple choices.
     * @param renderable list of objects that have defined
     *                   certain representation
     * @return user selected item
     */
    Object select(List<? extends Renderable> renderable);

}
