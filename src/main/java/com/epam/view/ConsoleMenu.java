package com.epam.view;

import com.epam.model.Renderable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The {@code ConsoleView} class represents menu
 * that is displayed in console.
 */
public class ConsoleMenu implements Menu {

    /**
     * Menu items.
     */
    private Map<Integer, String> menu = new LinkedHashMap<>();

    public final String enter(Renderable renderable) {
        System.out.println(renderable.render());
        return readFromConsole("");
    }

    public final Object select(List<? extends Renderable> renderable) {
        final AtomicInteger counter = new AtomicInteger();
        renderable
                .stream()
                .peek(r -> System.out.print(counter.get() + " - "))
                .peek(r -> menu.put(counter.getAndIncrement(), r.render()))
                .forEach(r -> System.out.println(r.render()));

        return renderable.get(
                Integer.parseInt(
                        readFromConsole("Enter you choice: ")));
    }

    /**
     * Reads input form user.
     * @param question represents message that will be
     *                 displayed before user input
     * @return user input
     */
    private String readFromConsole(String question) {
        String userInput = null;
        boolean waitInput = true;
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        while (waitInput) {
            try {
                System.out.print(question);
                userInput = br.readLine();
                waitInput = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return userInput;
    }

}

