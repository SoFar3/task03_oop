package com.epam.model;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Port} class represents port.
 */
public class Port implements Renderable {

    /**
     * Port name.
     */
    private String name;

    /**
     * Max weight that port can handle.
     */
    private int maxWeight;

    /**
     * List of suppliers that supplies shipments
     * to this port.
     */
    private List<Delivery> suppliers;

    /**
     * Price per 1 kilo of shipment.
     */
    private double price;

    /**
     * Constructs the Port.
     * @param name of port
     */
    public Port(final String name) {
        this.name = name;
        this.suppliers = new ArrayList<>();
    }

    /**
     * Constructs the port.
     * @param name of the port
     * @param maxWeight that port can handle
     * @param price per 1 kilo
     */
    public Port(final String name, final int maxWeight, final Double price) {
        this.name = name;
        this.price = price;
        this.maxWeight = maxWeight;
    }

    /**
     * Setter.
     * @param suppliers
     */
    public final void setSuppliers(final List<Delivery> suppliers) {
        this.suppliers = suppliers;
    }

    /**
     * Getter.
     * @return name
     */
    public final String getName() {
        return name;
    }

    /**
     * Getter.
     * @return suppliers
     */
    public final List<Delivery> getSuppliers() {
        return suppliers;
    }

    /**
     * Getter.
     * @return price
     */
    public final double getPrice() {
        return price;
    }

    /**
     * Getter.
     * @return max weight
     */
    public final int getMaxWeight() {
        return maxWeight;
    }

    @Override
    public String render() {
        return name;
    }

}
