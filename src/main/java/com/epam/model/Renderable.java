package com.epam.model;

/**
 * Implementing this interface allows an object
 * to be suitable for menu displaying.
 */
@FunctionalInterface
public interface Renderable {

    /**
     * Returns string representation of object.
     * @return string
     */
    String render();

}
