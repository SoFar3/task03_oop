package com.epam.model;

/**
 * The {@code Delivery} class represents port that
 * make deliveries.
 */
public class Delivery implements Renderable {

    /**
     * Time for delivery.
     */
    private int time;
    private Port supplier;

    /**
     * Constructs the Delivery.
     * @param time for delivery
     * @param destination {@code Port}
     */
    public Delivery(int time, Port destination) {
        this.time = time;
        this.supplier = destination;
    }

    /**
     * Getter.
     * @return time
     */
    public int getTime() {
        return time;
    }

    /**
     * Setter.
     * @param time for delivery
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * Getter.
     * @return {@code Port}
     */
    public Port getSupplier() {
        return supplier;
    }

    /**
     * Setter.
     * @param supplier
     */
    public void setSupplier(Port supplier) {
        this.supplier = supplier;
    }

    @Override
    public String render() {
        return " <-- " + supplier.getName() + " delivery time: "
                + time + "h price: " + supplier.getPrice() + "$/kg;";
    }

}
