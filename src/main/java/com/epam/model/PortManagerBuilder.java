package com.epam.model;

/**
 * Builder for {@code PortManager}.
 */
public class PortManagerBuilder {

    /**
     * {@code PortManager}.
     */
    private PortManager portManager = new PortManager();

    /**
     * Returns fully assembled {@code PortManager}.
     * @return {@code PortManager}
     */
    public PortManager build() {
        return portManager;
    }

    /**
     * Adds new port to {@code PortManager}.
     * @param port
     * @return {@code PortManagerBuilder}. This allows us to invoke
     * this method consistently.
     */
    public PortManagerBuilder addPort(final Port port) {
        portManager.addPort(port);
        return this;
    }

}
