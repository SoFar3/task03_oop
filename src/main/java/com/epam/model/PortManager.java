package com.epam.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@code PortManager} class contains all ports
 * and methods that for filtering ports
 * by weight and price.
 */
public class PortManager {

    /**
     * All ports.
     */
    private ArrayList<Port> ports;

    /**
     * Constructs the PortManager.
     */
    public PortManager() {
        ports = new ArrayList<>();
    }

    /**
     * Adds new port to list.
     * @param port that will be added to list
     */
    public final void addPort(final Port port) {
        ports.add(port);
    }

    /**
     * Getter.
     * @return list of {@code Port}
     */
    public final ArrayList<Port> getPorts() {
        return ports;
    }

    /**
     * Returns cheapest delivery for given Port
     * @param port {@code Port}
     * @return {@code Delivery}
     */
    public final Delivery cheapestDelivery(Port port) {
        return port.getSuppliers()
                .stream()
                .min(Comparator.comparingDouble(d -> d.getSupplier().getPrice()))
                .get();
    }

    /**
     * Returns fastest delivery for given Port
     * @param port {@code Port}
     * @return {@code Delivery}
     */
    public final Delivery fastestDelivery(Port port) {
        return port.getSuppliers()
                .stream()
                .min(Comparator.comparingInt(Delivery::getTime))
                .get();
    }

    /**
     * Returns list of delivery filtered for given port
     * by weight and price parameters.
     * @param port {@code Port}
     * @param weight in kilo
     * @param price per 1 kilo
     * @return list of {@code Delivery}
     */
    public final List<Delivery> availableDeliveries(
            final Port port,
            final Double weight,
            final Double price) {
        return port
                .getSuppliers()
                .stream()
                .filter(d ->
                        d.getSupplier().getPrice() <= price
                                && d.getSupplier().getMaxWeight() >= weight)
                .collect(Collectors.toList());
    }

}
