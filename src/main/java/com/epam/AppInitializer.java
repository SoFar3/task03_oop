package com.epam;

import com.epam.controller.Controller;
import com.epam.model.Delivery;
import com.epam.model.Port;
import com.epam.model.PortManagerBuilder;

import java.util.ArrayList;

/**
 * Main class.
 *
 * @author Yaroslav Teplyi
 */
public class AppInitializer {

    /**
     * Entry point of our app.
     * @param args initial parameters that can be pass to our app
     */
    public static void main(String[] args) {
        Controller controller = init();
        Double weight = Double.valueOf(controller.enterWeight());
        Double maxPrice = Double.valueOf(controller.enterMaxPrice());

        System.out.println("Choose the destination port: ");
        Port port = (Port) controller.chooseDestination();

        String option1 = "Find cheapest delivery";
        String option2 = "Find fastest delivery";
        String option3 = "View all deliveries";

        Integer choice = controller.chooseOption(option1, option2, option3);
        if (choice == 0) {
            controller.cheapestDelivery(port);
        } else if (choice == 1) {
            controller.fastestDelivery(port);
        } else {
            System.out.println("Choose the start port: ");
            Delivery delivery = controller
                    .availableDeliveries(port, weight, maxPrice);
            System.out.println(port.render() + delivery.render());
        }
    }

    /**
     * Initialize {@code Controller} with data.
     * @return fully assembled {@code Controller}
     */
    public static Controller init() {
        Port port1 = new Port("Port of Shanghai", 50000, 3.5);
        Port port2 = new Port("Port of Singapore", 500000, 3.6);
        Port port3 = new Port("Port of Shenzhen", 366120, 4.0);
        Port port4 = new Port("Port of Busan", 100000, 3.7);
        Port port5 = new Port("Port of Qingdao", 1_000_000, 4.2);
        Port port6 = new Port("Port of Rotterdam", 5_000_000, 3.1);

        ArrayList<Delivery> deliveries1 = new ArrayList<>();
        deliveries1.add(new Delivery(80, port2));
        deliveries1.add(new Delivery(65, port3));
        deliveries1.add(new Delivery(90, port6));
        port1.setSuppliers(deliveries1);

        ArrayList<Delivery> deliveries2 = new ArrayList<>();
        deliveries2.add(new Delivery(120, port1));
        deliveries2.add(new Delivery(100, port4));
        deliveries2.add(new Delivery(85, port3));
        port2.setSuppliers(deliveries2);

        ArrayList<Delivery> deliveries3 = new ArrayList<>();
        deliveries3.add(new Delivery(50, port2));
        deliveries3.add(new Delivery(24, port5));
        deliveries3.add(new Delivery(60, port6));
        port3.setSuppliers(deliveries3);

        ArrayList<Delivery> deliveries4 = new ArrayList<>();
        deliveries4.add(new Delivery(200, port1));
        deliveries4.add(new Delivery(160, port4));
        deliveries4.add(new Delivery(120, port3));
        port4.setSuppliers(deliveries4);

        ArrayList<Delivery> deliveries5 = new ArrayList<>();
        deliveries5.add(new Delivery(30, port2));
        deliveries5.add(new Delivery(21, port3));
        deliveries5.add(new Delivery(42, port6));
        port5.setSuppliers(deliveries5);

        ArrayList<Delivery> deliveries6 = new ArrayList<>();
        deliveries6.add(new Delivery(185, port2));
        deliveries6.add(new Delivery(171, port3));
        deliveries6.add(new Delivery(204, port6));
        deliveries6.add(new Delivery(153, port5));
        port6.setSuppliers(deliveries6);

        return new Controller(
                new PortManagerBuilder()
                        .addPort(port1)
                        .addPort(port2)
                        .addPort(port3)
                        .addPort(port4)
                        .addPort(port5)
                        .addPort(port6)
                        .build()
        );
    }

}
