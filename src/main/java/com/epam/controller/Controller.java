package com.epam.controller;

import com.epam.model.Delivery;
import com.epam.model.Port;
import com.epam.model.PortManager;
import com.epam.model.Renderable;
import com.epam.view.ConsoleMenu;
import com.epam.view.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Controller} class is connecting
 * Model and View.
 */
public class Controller {

    /**
     * Port manager.
     */
    private PortManager portManager;

    /**
     * View.
     */
    private Menu view;

    /**
     * Constructs the controller.
     * @param portManager port manager
     */
    public Controller(PortManager portManager) {
        this.portManager = portManager;
        view = new ConsoleMenu();
    }

    /**
     * Tells view to make single input menu
     * for weight param.
     * @return user input
     */
    public String enterWeight() {
        return view.enter(() -> "Enter shipment weight: ");
    }

    /**
     * Tells view to make single input menu
     * for maximum price param.
     * @return user input
     */
    public String enterMaxPrice() {
        return view.enter(() -> "Enter max price: ");
    }

    /**
     * Tells view to make select menu
     * for destination port.
     * @return user choice
     */
    public Object chooseDestination() {
        return view.select(portManager.getPorts());
    }

    /**
     * Tells view to make select menu
     * with custom options.
     * @param options custom menu items
     * @return user choice
     */
    public Integer chooseOption(String... options) {
        List<Renderable> optionsList = new ArrayList<>();
        for (String option : options) {
            optionsList.add(() -> option);
        }
        return optionsList.indexOf(view.select(optionsList));
    }

    public Port cheapestDelivery(Port port) {
        Delivery delivery = portManager.cheapestDelivery(port);
        view.enter(() -> port.getName() + delivery.render());
        return delivery.getSupplier();
    }

    public Port fastestDelivery(Port port) {
        Delivery delivery = portManager.fastestDelivery(port);
        view.enter(() -> port.getName() + delivery.render());
        return delivery.getSupplier();
    }

    public Delivery availableDeliveries(Port port, Double weight, Double price) {
        List<Delivery> collect = portManager.availableDeliveries(port, weight, price);
        if (collect.size() == 0) {
            throw new IllegalArgumentException("There is no suitable port for this parameters");
        }
        return ((Delivery)view.select(collect));
    }

}
